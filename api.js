'use strict';

var rateLimit = require('express-rate-limit');
var logger = require('./utils/logger')(module.filename);
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var dbConnection = require('./utils/database')(logger);

var registerRoutes = function(router, route, routesFiles, absoluteContextPath) {
	router.use(route, require(routesFiles)(absoluteContextPath + route));
};

module.exports = function(app, contextPath) {
	
	var router = express.Router();
	router.use(cors());
	router.use(new rateLimit({ windowMs: 15*60*1000, max: 1000, delayMs: 0 }));
	router.use(bodyParser.urlencoded({ extended: true }));
	router.use(bodyParser.json());
	
	require('./routers/baseRouter')(router, '/');
	require('./routers/authentificationRouter')(router, '/authentification');
	require('./routers/usersRouter')(router, '/users');
	require('./routers/groupsRouter')(router, '/groups');
	require('./routers/projectsRouter')(router, '/projects');
	require('./routers/appointmentsRouter')(router, '/appointments');
	require('./routers/availabilitiesRouter')(router, '/availabilities');
	
	router.use(function(req, res) {
		res.status(404).send({ url: 'Method ' + req.method + ' not allowed for ' + req.originalUrl })
	});
	
	dbConnection.connect(function(err) {
		
		if(err) {
			logger.error('API v1 BDD error', err);
			return;
		}
		app.use(contextPath, router);
		logger.success('API v1 listening on ' + contextPath);
		dbConnection.close();
	});
};