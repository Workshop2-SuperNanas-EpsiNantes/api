'use strict';

module.exports = function(router, path) {
	
	var jwt = require('../controllers/jwtController');
	var controller = require('../controllers/appointmentsController');
	
	router.route(path + '/')
		.get(jwt.jwt_authentification_required, controller.get_appointments);

	router.route(path + '/:appointment_id')
		.get(jwt.jwt_authentification_required, controller.get_appointment);
		
	return router;
};