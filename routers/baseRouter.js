'use strict';

module.exports = function(router, path) {
	
	var controller = require('../controllers/routesController');
	
	router.route(path + '/')
		.get(controller.get_default_links);
		
	return router;
};
