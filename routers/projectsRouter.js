'use strict';

module.exports = function(router, path) {
	
	var jwt = require('../controllers/jwtController');
	var controller = require('../controllers/projectsController');
	
	router.route(path + '/')
		.get(jwt.jwt_authentification_required, controller.get_projects);

	router.route(path + '/:project_id')
		.get(jwt.jwt_authentification_required, controller.get_project);

	router.route(path + '/create')
		.post(jwt.jwt_authentification_required, controller.create_project);
		
	return router;
};
