'use strict';

module.exports = function(router, path) {
	
	var jwt = require('../controllers/jwtController');
	var controller = require('../controllers/groupsController');
	var appointementsController = require('../controllers/appointmentsController');
	
	router.route(path + '/')
		.get(jwt.jwt_authentification_required, controller.get_groups);

	router.route(path + '/:group_id')
		.get(jwt.jwt_authentification_required, controller.get_group);

	router.route(path + '/:group_id/create_appointement')
		.post(jwt.jwt_authentification_required, appointementsController.create_appointement);
		
	return router;
};
