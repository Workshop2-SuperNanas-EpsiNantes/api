'use strict';

module.exports = function(router, path) {
	
	var jwt = require('../controllers/jwtController');
	var controller = require('../controllers/availabilitiesController');
	
	router.route(path + '/')
		.get(jwt.jwt_authentification_required, controller.get_availabilities);

	router.route(path + '/:availability_id')
		.get(jwt.jwt_authentification_required, controller.get_availability)
		
	return router;
};