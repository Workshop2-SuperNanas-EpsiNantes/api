'use strict';

module.exports = function(router, path) {
	
	var controller = require('../controllers/authentificationController');
	
	router.route(path + '/login')
		.post(controller.login);
		
	return router;
};