'use strict';

module.exports = function(router, path) {
	
	var jwt = require('../controllers/jwtController');
	var controller = require('../controllers/usersController');
	
	router.route(path + '/')
		.get(jwt.jwt_authentification_required, controller.get_users);

	router.route(path + '/:user_id')
		.get(jwt.jwt_authentification_required, controller.get_user)
		
	return router;
};
