'use strict';

var colors = {
	reset: '\x1b[0m',
	
	red: '\x1b[31m',
	green: '\x1b[32m',
	yellow: '\x1b[33m',
	
	bgRed: '\x1b[41m',
	bgGreen: '\x1b[42m',
	bgYellow: '\x1b[43m'
};

var log = function(tag, color, args) {
	
	args = Array.prototype.slice.call(args);
	args.splice(0, 0, '[' + new Date().toLocaleString() + '][' + tag + ']' + color);
	args.push(colors.reset);
	
	console.log.apply(this, args);
};

module.exports = function(filename) {
	
	filename = filename || module.parent.filename;
	
	var tag = filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1);
	
	return {
		debug: function() {
			log(tag, colors.reset, arguments);
		},
		warning: function() {
			log(tag, colors.yellow, arguments);
		},
		success: function() {
			log(tag, colors.green, arguments);
		},
		error: function() {
			log(tag, colors.red, arguments);
		}
	}
};