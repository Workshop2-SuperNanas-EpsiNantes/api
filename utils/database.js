'use strict';

var mysql = require('mysql2');

module.exports = function(logger) {
	
	if(logger) {
		logger.debug('new dbconnection');
	} else {
		console.log('No logger (' + module.filename + '/' + module.parent.filename + ') : new dbConnection');
	}
	
	var logger = require('./logger')(module.parent.filename);
	
	var dbConnection = mysql.createConnection({
		host: 'localhost',
		user: 'xxxx',
		password: 'xxxx',
		database: 'workshop2'
	});
	
	dbConnection.on('error', function(err) {
		if(logger) {
			logger.debug('DB Error', err);
		} else {
			console.log('No logger (' + module.filename + '/' + module.parent.filename + ') : DB Error', err);
		}
	});
	
	return dbConnection;
};