'use strict';

var logger = require('./utils/logger')(module.filename);

var port = process.env.PORT || 4201;

var app = require('express')();
require('./api')(app, '/v1');

app.enable('trust proxy');
app.listen(port, function() {
	logger.success('Server listening on 0.0.0.0:' + port);
});
