'use strict';
	
var logger = require('./../utils/logger')(module.filename);
var fs = require('fs');
var cert = fs.readFileSync('./certs/public.pem');
var jsonwebtoken = require('jsonwebtoken');

module.exports.jwt_authentification = function(req, res, next) {
	
	if (!req.headers || !req.headers.authorization) {
		req.jwtError = {message: 'No authentification token provided'};
		next();
		return;
	}
	
	if (req.headers.authorization.split(' ')[0] !== 'JWT') {
		req.jwtError = {message: 'Bad authentification token type'};
		next();
		return;
	}
		
	jsonwebtoken.verify(req.headers.authorization.split(' ')[1], cert, { algorithms: ['RS256'] }, function (err, payload) {
		
		if(err) {
			logger.warning('Bad JWT token: ' + req.headers.authorization);
			req.jwtError = {message: 'Bad authentification token: ' + err.name};
			next();
			return;
		}
		
		if(!payload.userId) {
			logger.error('Validated JWT token with bad format: ' + req.headers.authorization);
			req.jwtError = {message: 'Bad authentification token'};
			next();
			return;
		}
		
		req.user = {
			userId: payload.userId,
			userType: payload.userType
		};
		
		next();
	});
};

module.exports.jwt_authentification_required = function(req, res, next) {
	
	var callback = function() {
		
		if(!req.user) {
			res.status(401).json({message: (req.jwtError && req.jwtError.message? req.jwtError.message : 'Unauthorized') });
			return;
		}
		
		next();
	};
	
	module.exports.jwt_authentification(req, res, callback);
};
