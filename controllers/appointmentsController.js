'use strict';

var logger = require('./../utils/logger')(module.filename);
var guard = require('./../controllers/guardController');
var database = require('./../utils/database');

module.exports.get_appointments = function(req, res) {
	
	var query = 'SELECT a.id, a.message, a.user_id, a.group_id, a.start_at, a.end_at, a.cost, a.status, g.name as group_name, t.username as teacher_username  FROM `appointments` as a INNER JOIN groups as g on a.group_id = g.id INNER JOIN users as t on a.user_id = t.id';
	var queryParams = [];
	switch (req.user.userType) {
		case 1:
			break;
		case 2:
			query += ' WHERE `user_id` = ?';
			queryParams = [req.user.userId];
			break;
		case 3:
			query += ' INNER JOIN groups_members as gm on a.group_id = gm.group_id WHERE gm.user_id = ?';
			queryParams = [req.user.userId];
			break;
		default:
			logger.error('Bad user type');
			res.status(500).json({message: 'Unknow error'});
			return;
	}
	
	var dbConnection = database(logger);
	dbConnection.execute(
		query,
		queryParams,
		function(err, results, fields) {
			
			dbConnection.close();
			
			if(err) {
				logger.error(err);
				res.status(500).json({message: 'Unknow error'});
				return;
			}
			
			var appointments = [];

			for (var i = 0; i < results.length; i++) {
				var result = results[i];

				appointments[appointments.length] = {
					id: result.id,
					message: result.message,
					group_id: result.group_id,
					user_id: result.user_id,
					start_at: result.start_at,
					end_at: result.end_at,
					cost: result.cost,
					status: result.status,
					group_name: result.group_name,
					teacher_username: result.teacher_username
				};
			}
	
			res.json(appointments);
		}
	);
};

module.exports.get_appointment = function(req, res) {
	
	if(guard.params(req, res, {appointment_id: 'string'})) return;

	var dbConnection = database(logger);
	dbConnection.execute(
		'SELECT id, message, user_id, group_id, start_at, end_at FROM `appointments` WHERE `id` = ? LIMIT 1',
		[req.params.appointment_id], 
		function(err, results, fields) {
			
			dbConnection.close();
			
			if(err) {
				logger.error(err);
				res.status(500).json({message: 'Unknow error'});
				return;
			}
			
			if(!results.length) {
				res.status(404).json({message: 'Appointment not found for id: ' + user_id});
				return;
			}
			
			res.json({
				id: result.id,
				message: result.message,
				group_id: result.group_id,
				user_id: result.user_id,
				start_at: result.start_at,
				end_at: result.end_at
			});
		}
	);
};

module.exports.create_appointement = function(req, res) {
	
	if(guard.params(req, res, {group_id: 'string'})) return;
	if(guard.body(req, res, {message: 'string', user_id: 'string', start_at: 'string', end_at: 'string'})) return;

	var dbConnection = database(logger);
	dbConnection.execute(
		'SELECT id, name, credits_count FROM `groups` WHERE `id` = ? LIMIT 1',
		[req.params.group_id], 
		function(err, results, fields) {
			
			dbConnection.close();
			
			if(err) {
				dbConnection.close();
				logger.error(err);
				res.status(500).json({message: 'Unknow error'});
				return;
			}
			
			if(!results.length) {
				dbConnection.close();
				res.status(404).json({message: 'Group not found for id: ' + group_id});
				return;
			}
			
			var group = {
				id: results[0].id,
				username: results[0].name,
				credits_count: results[0].credits_count
			};
			
			var startDate = new Date(req.body.start_at);
			var endDate = new Date(req.body.end_at);
			var cost = (endDate.getTime()-startDate.getTime())/(10*60*1000);
			
			if(cost > group.credits_count) {
				res.status(400).json({message: 'Not enough credits'});
				return;
			}

			var dbConnection2 = database(logger);
			dbConnection2.execute(
				'INSERT INTO `appointments` (message, group_id, user_id, start_at, end_at, cost, status) VALUES (?, ?, ?, ?, ?, ?, 0)',
				[req.body.message, req.params.group_id, req.body.user_id, req.body.start_at, req.body.end_at, cost], 
				function(err, results, fields) {
					
					dbConnection2.close();
					
					if(err || results.affectedRows < 1) {
						logger.error(err);
						res.status(500).json({message: 'Unknow error'});
						return;
					}
					
					console.log((group.credits_count-cost));
					
					var dbConnection3 = database(logger);
					dbConnection3.execute(
						'UPDATE `groups` SET credits_count = ? WHERE `id` = ?',
						[(group.credits_count-cost)+'', req.params.group_id], 
						function(err, results, fields) {
							
							dbConnection3.close();
							
							if(err || results.affectedRows < 1) {
								logger.error(err, 'Warning: bad decrease group id ' + req.params.group_id + 'with cost ' + cost + '(before balancy ' + group.credits_count + ')');
								res.status(500).json({message: 'Unknow error'});
								return;
							}
							
							res.status(204).send();
						}
					);
				}
			);
		}
	);
}