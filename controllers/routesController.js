'use strict';

module.exports.get_default_links = function(req, res) {
	
	res.json({
		links: {
			login: '/authentification/login'
		}
	});
};
