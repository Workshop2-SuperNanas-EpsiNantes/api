'use strict';

var logger = require('./../utils/logger')(module.filename);
var guard = require('./../controllers/guardController');
var database = require('./../utils/database');

module.exports.get_availabilities = function(req, res) {
	
	var dbConnection = database(logger);
	dbConnection.execute(
		'SELECT a.id, a.user_id, a.available_from, a.available_to, u.username as teacher_username FROM `availabilities` as a INNER JOIN users as u on a.user_id = u.id',
		function(err, results, fields) {
			
			dbConnection.close();
			
			if(err) {
				logger.error(err);
				res.status(500).json({message: 'Unknow error'});
				return;
			}
			
			var availabilities = [];

			for (var i = 0; i < results.length; i++) {
				var result = results[i];

				availabilities[availabilities.length] = {
					id: result.id,
					user_id: result.user_id,
					available_from: result.available_from,
					available_to: result.available_to,
					teacher_username: result.teacher_username
				};
			}
	
			res.json(availabilities);
		}
	);
};

module.exports.get_availability = function(req, res) {
	
	if(guard.params(req, res, {availability_id: 'string'})) return;

	var dbConnection = database(logger);
	dbConnection.execute(
		'SELECT id, user_id, available_from, available_to FROM `availabilities` WHERE `id` = ? LIMIT 1',
		[req.params.availability_id], 
		function(err, results, fields) {
			
			dbConnection.close();
			
			if(err) {
				logger.error(err);
				res.status(500).json({message: 'Unknow error'});
				return;
			}
			
			if(!results.length) {
				res.status(404).json({message: 'Appointment not found for id: ' + user_id});
				return;
			}
			
			res.json({
				id: result.id,
				project_id: result.project_id,
				user_id: result.user_id,
				start_at: result.start_at,
				end_at: result.end_at
			});
		}
	);
};