'use strict';

var logger = require('./../utils/logger')(module.filename);
var guard = require('./../controllers/guardController');
var database = require('./../utils/database');

module.exports.get_projects = function(req, res) {
	
	var query = 'SELECT p.id, p.name, p.start_at, p.end_at, p.default_credits_count, p.max_groups_members FROM projects as p';
	var queryParams = [];
	switch (req.user.userType) {
		case 1:
			break;
		case 2:
			query += ' INNER JOIN groups as g on p.id = g.project_id INNER JOIN appointments as a on g.id = a.group_id WHERE a.user_id = ? GROUP BY g.id';
			queryParams = [req.user.userId];
			break;
		case 3:
			query += ' INNER JOIN groups as g on p.id = g.project_id INNER JOIN groups_members as gm on g.id = gm.group_id WHERE gm.user_id = ?';
			queryParams = [req.user.userId];
			break;
		default:
			logger.error('Bad user type');
			res.status(500).json({message: 'Unknow error'});
			return;
	}

	var dbConnection = database(logger);
	dbConnection.execute(
		query,
		queryParams,
		function(err, results, fields) {
			
			dbConnection.close();
			
			if(err) {
				logger.error(err);
				res.status(500).json({message: 'Unknow error'});
				return;
			}
			
			var projects = [];

			for (var i = 0; i < results.length; i++) {
				var result = results[i];

				projects[i] = {
					id: result.id,
					name: result.name,
					start_at: result.start_at,
					end_at: result.end_at,
					default_credits_count: result.default_credits_count,
					max_groups_members: result.max_groups_members
				};
			}
	
			res.json(projects);
		}
	);
};

module.exports.get_project = function(req, res) {
	
	if(guard.params(req, res, {project_id: 'string'})) return;
	
	var dbConnection = database(logger);
	dbConnection.execute(
		'SELECT p.id, p.name, p.start_at, p.end_at, p.default_credits_count, p.max_groups_members FROM projects p WHERE `id` = ? LIMIT 1',
		[req.params.project_id], 
		function(err, results, fields) {
			
			dbConnection.close();
			
			if(err) {
				logger.error(err);
				res.status(500).json({message: 'Unknow error'});
				return;
			}
			
			if(!results.length) {
				res.status(404).json({message: 'Project not found for id: ' + project_id});
				return;
			}

			res.json({
				id: results[0].id,
				name: results[0].name,
				start_at: results[0].start_at,
				end_at: results[0].end_at,
				default_credits_count: results[0].default_credits_count,
				max_groups_members: results[0].max_groups_members
			});
		}
	);
};

module.exports.create_project = function(req, res) {
	
	if(guard.body(req, res, {name: 'string', start_at: 'string', end_at: 'string', default_credits_count : 'number', max_groups_members : 'number'})) return;
	
	var dbConnection = database(logger);
	dbConnection.execute(
		'INSERT INTO `projects` (`name`, `start_at`, `end_at`, `default_credits_count`, `max_groups_members`) VALUES (?, ?, ?)',
		[req.body.name, req.body.start_at, req.body.end_at, req.body.default_credits_count, req.body.max_groups_members], 
		function(err, results, fields) {
			
			dbConnection.close();
			
			if(err) {
				logger.error(err);
				res.status(500).json({message: 'Unknow error'});
				return;
			}
			
			if(!results.length) {
				res.status(404).json({message: 'Impossible to create project ' + req.body.name});
				return;
			}

			res.json({
				id: results[0].id,
				name: results[0].name,
				start_at: results[0].start_at,
				end_at: results[0].end_at,
				default_credits_count: result[0].default_credits_count,
				max_groups_members: result[0].max_groups_members
			});
		}
	);
};