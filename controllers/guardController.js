'use strict';

var guard = function(req, res, obj, keys) {
	
	for(var key in keys) {
		
		if(typeof obj[key] !== keys[key]) {
			
			res.status(400).json({message: 'Bad Request: no value for ' + key, obj: obj});
			return true;
		}
	}
	
	return false;
};

module.exports = {
	
	body: function(req, res, keys) {
		
		return guard(req, res, req.body, keys);
	},
	params: function(req, res, keys) {
		
		return guard(req, res, req.params, keys);
	}
};
