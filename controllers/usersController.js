'use strict';

var logger = require('./../utils/logger')(module.filename);
var guard = require('./../controllers/guardController');
var database = require('./../utils/database');

module.exports.get_users = function(req, res) {
	
	var dbConnection = database(logger);
	dbConnection.execute(
		'SELECT id, username, email, type FROM `users`',
		function(err, results, fields) {
			
			dbConnection.close();
			
			if(err) {
				logger.error(err);
				res.status(500).json({message: 'Unknow error'});
				return;
			}
			
			var users = [];

			for (var i = 0; i < results.length; i++) {
				var result = results[i];

				users[users.length] = {
					id: result.id,
					username: result.username,
					email: result.email,
					type: result.type
				};
			}
	
			res.json(users);
		}
	);
};

module.exports.get_user = function(req, res) {
	
	if(guard.params(req, res, {user_id: 'string'})) return;

	var dbConnection = database(logger);
	dbConnection.execute(
		'SELECT id, username, email, type FROM `users` WHERE `id` = ? LIMIT 1',
		[req.params.user_id], 
		function(err, results, fields) {
			
			dbConnection.close();
			
			if(err) {
				logger.error(err);
				res.status(500).json({message: 'Unknow error'});
				return;
			}
			
			if(!results.length) {
				res.status(404).json({message: 'User not found for id: ' + user_id});
				return;
			}
			
			res.json({
				id: results[0].id,
				username: results[0].username,
				email: results[0].email,
				type: results[0].type
			});
		}
	);
};