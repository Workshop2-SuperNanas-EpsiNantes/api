'use strict';

var logger = require('./../utils/logger')(module.filename);
var guard = require('./../controllers/guardController');
var database = require('./../utils/database');

module.exports.get_groups = function(req, res) {
	
	var query = 'SELECT g.id, g.name, g.credits_count, g.project_id, p.name as project_name FROM `groups` as g INNER JOIN projects as p on g.project_id = p.id';
	var queryParams = [];
	switch (req.user.userType) {
		case 1:
			break;
		case 2:
			query += ' INNER JOIN appointments as a on g.id = a.group_id WHERE a.user_id = ? GROUP BY g.id';
			queryParams = [req.user.userId];
			break;
		case 3:
			query += ' INNER JOIN groups_members as gm on g.id = gm.group_id WHERE gm.user_id = ?';
			queryParams = [req.user.userId];
			break;
		default:
			logger.error('Bad user type');
			res.status(500).json({message: 'Unknow error'});
			return;
	}
	
	var dbConnection = database(logger);
	dbConnection.execute(
		query,
		queryParams,
		function(err, results, fields) {
			
			dbConnection.close();
			
			if(err) {
				logger.error(err);
				res.status(500).json({message: 'Unknow error'});
				return;
			}
			
			var groups = [];

			for (var i = 0; i < results.length; i++) {
				var result = results[i];

				groups[groups.length] = {
					id: result.id,
					name: result.name,
					credits_count: result.credits_count,
					project_id: result.project_id,
					project_name: result.project_name
				};
			}
	
			res.json(groups);
		}
	);
};

module.exports.get_group = function(req, res) {
	
	if(guard.params(req, res, {group_id: 'string'})) return;

	var dbConnection = database(logger);
	dbConnection.execute(
		'SELECT id, name, credits_count FROM `groups` WHERE `id` = ? LIMIT 1',
		[req.params.group_id], 
		function(err, results, fields) {
			
			dbConnection.close();
			
			if(err) {
				logger.error(err);
				res.status(500).json({message: 'Unknow error'});
				return;
			}
			
			if(!results.length) {
				res.status(404).json({message: 'Group not found for id: ' + group_id});
				return;
			}
			
			res.json({
				id: results[0].id,
				username: results[0].name,
				credits_count: results[0].credits_count
			});
		}
	);
};