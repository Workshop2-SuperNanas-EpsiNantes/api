'use strict';

var fs = require('fs');
var cert = fs.readFileSync('./certs/private.pem');
var jsonwebtoken = require('jsonwebtoken');
var guard = require('./../controllers/guardController');
var logger = require('./../utils/logger')(module.filename);
var database = require('./../utils/database');
var passwordHash = require('password-hash');

var login = function(req, res, identifier, password) {
		
	var dbConnection = database(logger);
	dbConnection.execute(
		'SELECT id, password, type FROM `users` WHERE `email` = ? OR `username` = ? LIMIT 1',
		[identifier, identifier], 
		function(err, results, fields) {
			
			dbConnection.close();
			
			if(err) {
				logger.error(err);
				res.status(500).json({message: 'Unknow error'});
				return;
			}
			
			if(!results.length || !passwordHash.verify(password, results[0].password)) {
				res.status(401).json({message: 'Bad identifier and/or password'});
				return;
			}
			
			var userId = results[0].id;
			var userType = results[0].type;
	
			jsonwebtoken.sign(
				{
					exp: Math.floor(Date.now()/1000) + (60*60),
					userId: userId,
					userType: userType
				}, cert, { algorithm: 'RS256' }, function(err, token) {
				
				if(err) {
					logger.error(err);
					res.status(500).json({message: 'Unknow authentification error'});
					return;
				}
				
				res.json({
					sessionToken: token,
					links: {
						logout: '/authentification/logout',
						projects: '/projects',
						groups: '/groups',
						users: '/users',
						appointments: '/appointments',
						availabilities: '/availabilities'
					}
				});
				
			});
		}
	);
};

module.exports.login = function(req, res) {
	
	if(guard.body(req, res, {identifier: 'string', password: 'string'})) return;
	
	var identifier = req.body.identifier;
	var password = req.body.password;
	
	login(req, res, identifier, password);
};

module.exports.register = function(req, res) {
	
	if(guard.body(req, res, {username: 'string', email: 'string', password: 'string'})) return;
	
	var username = req.body.username;
	var email = req.body.email;
	var password = req.body.password;
	
	var hashedPassword = passwordHash.generate(password);
	
	var dbConnection = database(logger);
	dbConnection.execute(
		'INSERT into `users` (`username`, `email`, `password`, type) VALUES (?, ?, ?, 1)',
		[username, email, hashedPassword],
		function(err, results, fields) {
			
			dbConnection.close();
			
			if(err) {
				logger.error(err);
				res.status(500).json({message: 'Unknow error'});
				return;
			}
			
			login(req, res, email, password);
		}
	);
};